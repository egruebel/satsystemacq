﻿using System;
using System.Reflection;
using System.Threading;

namespace SatSystemAcq
{
    class Program
    {
        
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                //show the user an error
                CriticalError("This application requires a minimum of two command line arguments...");
                return;
            }

            //process command line args
            try
            {
                var system = new object();
                switch (args[0])
                {
                    case "fbb500":
                        system = new FBB500Model(args[1]);
                        BeginAcq((ISatSystem)system);
                        break;
                    case "verizon":
                        system = new VerizonSystemModel(args[1], args[2]);
                        BeginAcq((ISatSystem) system);
                        break;
                    case "kvhv7ip":
                        system = new TracphoneModel(args[1]);
                        BeginAcq((ISatSystem)system);
                        break;
                    case "hiseasnet":
                        system = new HiSeasNetSystemModel(args[1],args[2]);
                        BeginAcq((ISatSystem)system);
                        break;
                    default:
                        Console.WriteLine("Unrecognized system " + args[0]);
                        return;
                 }
            }
            catch
            {
                CriticalError("Unable to process the supplied parameters");
                return;
            }

        }

        public static void BeginAcq(ISatSystem system)
        {
            
            for (; ; )
            {
                if (system.QuerySystemStatus())
                {
                    SatMonRepository.AddToDb(system);
                    PrintSystemProps(system);
                }
                else
                {
                    //wait a few seconds and try one more time
                    Thread.Sleep(1000 * 2); 
                    if (system.QuerySystemStatus())
                    {
                        SatMonRepository.AddToDb(system);
                        PrintSystemProps(system);
                    }
                }
                Thread.Sleep(1000 * 60 * 4); //four minute delay
            }
        }

        public static void PrintSystemProps(ISatSystem system)
        {
            Console.Clear();
            const string underline = "-----------------------------------";
            Console.WriteLine(underline);
            Console.WriteLine(system.GetType().Name);
            Console.WriteLine(underline);
            //Console.WriteLine(string.Format("Lat: {0}", system.Lat));
            foreach (PropertyInfo prop in system.GetType().GetProperties())
            {
                Console.WriteLine(prop.Name + ": " + prop.GetValue(system, null));
            }
        }

        public static void PrintError()
        {
            if(LastError != null)
                Console.WriteLine(LastError.Message + " " + LastError.StackTrace);
        }

        public static void CriticalError(string message)
        {
            Console.WriteLine(message);
            Console.WriteLine("This application requires a minimum of two command line arguments...");
            Console.WriteLine("System Type: can be either 'fbb500', 'kvhv7ip', 'verizon' or 'hiseasnet'");
            Console.WriteLine("Ip Address: the ip address of the system device");
            Console.WriteLine("Ip Address 2 (optional): only for the hiseasnet system, specify the modem ip");
        }

        public static SatSystemError LastError { get; set; }
        
        public static void LogError(string sysId, string system, string message, string error)
        {
            var newError = new SatSystemError()
            {
                System = system,
                Message = message,
                StackTrace = error 
            };
            if (LastError == null || LastError.Code != newError.Code)
            {
                //This is a new error
                var e = new ErrorLog() { 
                    DateTime = DateTime.Now,
                    SysId = sysId,
                    System = system,
                    Error = string.Empty,
                    StackTrace = error 
                };
                var db = new SatMonDbDataClassesDataContext();
                db.ErrorLogs.InsertOnSubmit(e);
                db.SubmitChanges();
                Console.WriteLine(string.Format("Last Error: {0}--{1}: {2}", e.DateTime.ToString(), e.System, e.StackTrace));
            }
            
        }

    }
}
