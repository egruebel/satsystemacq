﻿using System;

namespace SatSystemAcq
{
    class SatMonRepository
    {
        public static void AddToDb(ISatSystem sys)
        {
            var sysType = sys.GetType().Name;
            using (var db = new SatMonDbDataClassesDataContext())
            {
                switch (sysType)
                {
                    case "FBB500Model":
                        var i = new FBB500()
                        {
                            DateTime = DateTime.Now,
                            Lat = sys.Lat,
                            Lon = sys.Lon,
                            Hdg = sys.Hdg,
                            Rel = sys.Rel,
                            SatLon = sys.SatLon,
                            EbNo = sys.EbNo,
                            Azimuth = sys.Azimuth,
                            Elevation = sys.Elevation,
                            CrossPol = sys.CrossPol,
                            SatName = sys.SatName,
                            SigStrength = sys.SigStrength,
                            ConStatus = sys.ConStatus
                        };
                        db.FBB500s.InsertOnSubmit(i);
                        db.SubmitChanges();
                        break;
                    case "HiSeasNetSystemModel":
                        var j = new HiSeasNet()
                        {
                            DateTime = DateTime.Now,
                            Lat = sys.Lat,
                            Lon = sys.Lon,
                            Hdg = sys.Hdg,
                            Rel = sys.Rel,
                            SatLon = sys.SatLon,
                            EbNo = sys.EbNo,
                            Azimuth = sys.Azimuth,
                            Elevation = sys.Elevation,
                            CrossPol = sys.CrossPol,
                            SatName = sys.SatName,
                            SigStrength = sys.SigStrength,
                            ConStatus = sys.ConStatus
                        };
                        db.HiSeasNets.InsertOnSubmit(j);
                        db.SubmitChanges();
                        break;
                    case "VerizonSystemModel":
                        var l = new Verizon()
                        {
                            DateTime = DateTime.Now,
                            Lat = sys.Lat,
                            Lon = sys.Lon,
                            Hdg = sys.Hdg,
                            Rel = sys.Rel,
                            SatLon = sys.SatLon,
                            EbNo = sys.EbNo,
                            Azimuth = sys.Azimuth,
                            Elevation = sys.Elevation,
                            CrossPol = sys.CrossPol,
                            SatName = sys.SatName,
                            SigStrength = sys.SigStrength,
                            ConStatus = sys.ConStatus
                        };
                        db.Verizons.InsertOnSubmit(l);
                        db.SubmitChanges();
                        break;
                    case "TracphoneModel":
                        var k = new TracPhone()
                        {
                            DateTime = DateTime.Now,
                            Lat = sys.Lat,
                            Lon = sys.Lon,
                            Hdg = sys.Hdg,
                            Rel = sys.Rel,
                            SatLon = sys.SatLon,
                            EbNo = sys.EbNo,
                            Azimuth = sys.Azimuth,
                            Elevation = sys.Elevation,
                            CrossPol = sys.CrossPol,
                            SatName = sys.SatName,
                            SigStrength = sys.SigStrength,
                            ConStatus = sys.ConStatus
                        };
                        db.TracPhones.InsertOnSubmit(k);
                        db.SubmitChanges();
                        break;
                }
            }
        }
    }
}
