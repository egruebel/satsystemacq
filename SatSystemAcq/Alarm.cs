﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SatSystemAcq
{
    class SatSystemError
    {
        public string System { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }

        public int Code
        {
            get
            {
                return Message.GetHashCode() + StackTrace.GetHashCode();
            }
        }
    }
}
